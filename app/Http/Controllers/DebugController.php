<?php

namespace App\Http\Controllers;

use App\QuizCategory;
use Illuminate\Http\Request;

class DebugController extends Controller
{
    public function index()
    {

        $data['record']       	  = 1;
        $data['active_class']     = 'exams';
        $data['settings']         = FALSE;
        $data['categories']         = array_pluck(QuizCategory::all(), 'category', 'id');
        $data['title']            = getPhrase('edit_series');

        $view_name = getTheme().'::exams.examseries.sitelayout2';
        return $view_name;
        return view($view_name, $data);

    }
}
