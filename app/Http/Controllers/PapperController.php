<?php

namespace App\Http\Controllers;

use App\ImageSettings;
use App\QuestionBank;
use App\Quiz;
use App\Subject;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PapperController extends Controller
{
    public function index()
    {
        if (!checkRole(getUserGrade(2))) {
            prepareBlockUserMessage();
            return back();
        }

        $data['active_class'] = 'exams';
        $data['title'] = getPhrase('quizzes');
        // return view('exams.quiz.list', $data);

        $view_name = getTheme() . '::papers.list';
        return view($view_name, $data);
    }

    public function updateQuestions($slug)
    {
        if (!checkRole(getUserGrade(2))) {
            prepareBlockUserMessage();
            return back();
        }

        /**
         * Get the Quiz Id with the slug
         * Get the available questions from questionbank_quizzes table
         * Load view with this data
         */
        $record = Quiz::getRecordWithSlug($slug);
        $data['record'] = $record;
        $data['active_class'] = 'exams';
        // $data['right_bar']          = FALSE;
        // $data['right_bar_path']     = 'exams.quiz.right-bar-update-questions';

        $data['settings'] = FALSE;
        $previous_questions = array();

        if ($record->total_questions > 0) {
            $questions = DB::table('questionbank_quizzes')
                ->where('quize_id', '=', $record->id)
                ->get();
            // dd($questions);

            $totalMarks = $record->total_marks;
            foreach ($questions as $question) {
                $temp = array();
                $temp['id'] = $question->subject_id . $question->questionbank_id;
                $temp['subject_id'] = $question->subject_id;
                $temp['question_id'] = $question->questionbank_id;
                $temp['marks'] = $question->marks;
                $totalMarks= $totalMarks+$question->marks;
                $question_details = QuestionBank::find($question->questionbank_id);
                $subject = $question_details->subject;

                $temp['question'] = $question_details->question;
                $temp['question_type'] = $question_details->question_type;
                $temp['difficulty_level'] = $question_details->difficulty_level;
                $temp['subject_title'] = $subject->subject_title;
                array_push($previous_questions, $temp);
            }

            $section_data = [];

            $section_wise_questions = [];
            $settings['is_have_sections'] = 0;
            $settings['questions'] = $previous_questions;

            if ($record->exam_type != 'NSNT') {
                $settings['is_have_sections'] = 1;

                if ($record->section_data) {

                    $section_data = json_decode($record->section_data);
                }

                $temp_questions = [];

                foreach ($previous_questions as $question)
                    $temp_questions[$question['question_id']] = $question;


                foreach ($section_data as $sd) {
                    $index = str_replace(' ', '_', $sd->section_name);
                    $section_wise_questions[$index]['section_name'] = $sd->section_name;
                    $section_wise_questions[$index]['section_time'] = $sd->section_time;

                    foreach ($sd->questions as $q_no) {
                        $section_wise_questions[$index]['questions'][] = $temp_questions[$q_no];
                    }

                    $index++;
                }

                $settings['questions'] = $section_wise_questions;
            }

            $settings['total_marks'] =  $totalMarks;
            $settings['section_data'] = $record->section_data;
            $data['settings'] = json_encode($settings);
        }


        $data['subjects'] = array_pluck(Subject::all(), 'subject_title', 'id');
        $data['title'] = getPhrase('update_questions_for') . ' ' . $record->title;


        // return view('exams.quiz.update-questions', $data);

        $view_name = getTheme() . '::papers.update-questions';
        return view($view_name, $data);
    }

    public function createQuestions(Request $request)
    {

        if (!checkRole(getUserGrade(2))) {
            prepareBlockUserMessage();
            return back();
        }

        DB::beginTransaction();
        try {
            $question_type = 'radio';
            $record = new QuestionBank();
            $name = $request->question;
            $record->question = $name;
            $record->slug = $record->makeSlug(getHashCode());
            $record->subject_id = 1;
            $record->topic_id = 1;
            $record->question = $request->question;
            $record->difficulty_level = 1;
            $record->hint = 'null';
            $record->explanation = $request->explanation;
            $record->marks = $request->marks;
            $record->question_type = $question_type;
            $record->time_to_spend = $request->time_to_spend;


            $record->total_answers = 4;
            $record->correct_answers = $request->correct_answers;
            $record->total_correct_answers = 1;


            $record->save();

            $record->answers = $this->prepareOptions($request, $record);
            
            $record->save();

            $quiz = Quiz::find($request->quize_id);

            $added_sections = 'Section 1';
            $added_times = 60;

            $marks = $quiz->total_marks;
            $questions_to_update = array();
            $sections_data = array();


            $temp = array();
            $temp['subject_id'] = $record->subject_id;
            $temp['questionbank_id'] = $record->id;
            $temp['quize_id'] = $quiz->id;
            $temp['marks'] = $record->marks;
            $marks += $record->marks;

            array_push($questions_to_update, $temp);

            $key = str_replace(' ', '_', $added_sections);

            $sections_data[$key]['section_name'] = $added_sections;
            $sections_data[$key]['section_time'] = $added_times;


            $sections_data = json_encode($sections_data);

            $total_questions = count($questions_to_update);


            //Insert New Questions
            DB::table('questionbank_quizzes')->insert($questions_to_update);
            $quiz->total_questions = $total_questions;
            $quiz->total_marks = $marks;
            $quiz->section_data = $sections_data;
            $quiz->save();

            flash('success', 'record_added_successfully', 'success');
            DB::commit();


        } catch (Exception $e) {

            DB::rollBack();
            if (getSetting('show_foreign_key_constraint', 'module')) {

                flash('oops...!', $e->errorInfo, 'error');
            } else {
                flash('oops...!', 'improper_data_in_the_question', 'error');
            }
        }
        return redirect()->back();
    }

    public function prepareOptions($request, $record)
    {

        $options = $request->options;
        $optionsl2 = $request->optionsl2;
        $list = array();

        /**
         * Get the image path from ImageSettings class
         * This destinationPath variable will be used
         * to delete an image if user edits any question and changes an image
         */
        $imageObject = new ImageSettings();
        $destinationPath = $imageObject->getExamImagePath();

        $total_answers = sizeof(array_filter($options));


        /**
         * Loop the total options selected by user
         * and process each option by checking wether the image
         * has been uploaded or not
         * After this loop multiple objects will be created based on
         * the no. of options(total_answers) selected by user
         * Each object contains 3 properties
         * 1) option_value : stores the text submitted as option
         * 2) has_file     : stores if this particular option has any file
         * 3) file_name    : stores the name of file uploaded
         */
        for ($index = 0; $index < $total_answers; $index++) {
            /**
             * The $answers variable is used when user edit any question
             * It will contain the previous option values
             * As it is under for loop, every option property will be checked
             * by comparing wether the file is submitted for this particular object
             * If submitted it will delete the old file and overwrite with new file
             * @var [type]
             */
            $answers = json_decode($record->answers);
            $old_has_file = isset($answers[$index]->has_file) ? $answers[$index]->has_file : 0;
            $old_file_name = isset($answers[$index]->file_name) ? $answers[$index]->file_name : '';

            $spl_char = ['\t', '\n', '\b', '\c', '\r', '\'', '\\', '\$', '\"', "'"];
            $list[$index]['option_value'] = str_replace($spl_char, '', $options[$index]);
            $list[$index]['optionl2_value'] = str_replace($spl_char, '', $optionsl2[$index]);

            // $list[$index]['option_value'] 	= $options[$index];
            $list[$index]['has_file'] = $old_has_file;
            $list[$index]['file_name'] = $old_file_name;
            $file_name = 'upload_' . $index;
            if ($request->hasFile($file_name)) {
                $rules = array($file_name => 'mimes:jpeg,jpg,png,gif|max:10000');
                $validator = Validator::make($request->options, $rules);
                if ($validator->fails())
                    return '';

                //Delete Old Files
                if ($old_file_name)
                    $this->deleteExamFile($old_file_name, $destinationPath);

                // This option has the image to be uploaded,
                // so process image and update the fields
                $list[$index]['has_file'] = 1;
                $list[$index]['file_name'] = $this->processUpload($request, $record, $file_name, 'option');


            }

        }

        return json_encode($list);
    }
}
