<?php

namespace App\Http\Controllers;

use \App;
use Illuminate\Http\Request;
use App\SubCategory;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Controllers\Controller;
use Auth;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!checkRole(getUserGrade(2))) {
            prepareBlockUserMessage();
            return back();
        }

        $data['active_class'] = 'subcategory';
        $data['title'] = getPhrase('sub_category');
        // return view('exams.examseries.list', $data);
        $view_name = getTheme() . '::sub-category.list';
        return view($view_name, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!checkRole(getUserGrade(2))) {
            prepareBlockUserMessage();
            return back();
        }
        $data['record'] = FALSE;
        $data['active_class'] = 'subcategory';
        $data['title'] = getPhrase('add_subcategory');


        $data['sub_categories'] = array_pluck(App\SubCategory::whereNull('parent_id')->get(),
            'name', 'id');
        $view_name = getTheme() . '::sub-category.insert-subcategory';
        return view($view_name, $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!checkRole(getUserGrade(2))) {
            prepareBlockUserMessage();
            return back();
        }
// catimage
        $rules = [
            'name' => 'bail|required|max:60',
        ];
        $this->validate($request, $rules);
        $record = new SubCategory();
        $name = $request->name;
        $record->name = $name;
        $record->description = $request->description;
        $record->record_updated_by = Auth::user()->id;
        $record->save();


        flash('success', 'record_added_successfully', 'success');
        return redirect(URL_SUB_CATEGORIES);
    }

    public function storesub(Request $request)
    {
        if (!checkRole(getUserGrade(2))) {
            prepareBlockUserMessage();
            return back();
        }
// catimage
        $rules = [
            'parent_id' => 'numeric|required',
            'name' => 'bail|required|max:60',
        ];
        $this->validate($request, $rules);
        $record = new SubCategory();
        $record->parent_id = $request->parent_id;
        $name = $request->name;
        $record->name = $name;
        $record->description = $request->description;
        $record->record_updated_by = Auth::user()->id;
        $record->save();


        flash('success', 'record_added_successfully', 'success');
        return redirect(URL_SUB_CATEGORIES);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_SUB_CATEGORIES;
    }

    public function getDatatable()
    {

        if (!checkRole(getUserGrade(2))) {
            prepareBlockUserMessage();
            return back();
        }

        $records = array();
        $records = SubCategory::select(['id', 'name', 'description', 'updated_at'])
            ->orderBy('created_at', 'desc');

        return Datatables::of($records)
            ->addColumn('action', function ($records) {

                $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-dots-vertical"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel">
                            <li><a href="'.route('sub.create').'"><i class="fa fa-pencil"></i>' . getPhrase("edit") . '</a></li>';

                $temp = '';
                if (checkRole(getUserGrade(1))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\'' . $records->id . '\');"><i class="fa fa-trash"></i>' . getPhrase("delete") . '</a></li>';
                }

                $temp .= '</ul></div>';


                $link_data .= $temp;
                return $link_data;
            })
            ->removeColumn('id')
            ->removeColumn('updated_at')
            ->make();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
