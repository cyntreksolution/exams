<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = "sub_categories";

    public function mainCategory(){
        return $this->belongsTo(SubCategory::class,'parent_id');
    }
}
