<!DOCTYPE html>
<html lang="en">


<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ========== Page Title ========== -->
    <title> @yield('title') ExamHUB.lk</title>

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{IMAGE_PATH_SETTINGS.getSetting('site_favicon', 'site_settings')}}" type="image/x-icon"/>
    <!-- ========== Start Stylesheet ========== -->
    <link href="{{asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/font-awesome.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/flaticon-set.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/magnific-popup.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/owl.carousel.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/owl.theme.default.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/animate.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/bootsnav.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/css/responsive.css')}}" rel="stylesheet"/>
    <!-- ========== End Stylesheet ========== -->


    <!-- ========== Google Fonts ========== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">

</head>

<body>

<!-- Preloader Start -->
<div class="se-pre-con"></div>
<!-- Preloader Ends -->

<!-- Start Header Top
============================================= -->
<div class="top-bar-area address-two-lines bg-dark text-light">
    <div class="container">
        <div class="row">
            <div class="col-md-8 address-info">
                <div class="info box">
                    <ul>
                        {{--                        <li>--}}
                        {{--                            <span><i class="fas fa-map"></i> Address</span>California, TX 70240--}}
                        {{--                        </li>--}}
                        <li>
                            <span><i class="fas fa-envelope-open"></i> Email</span>
                            <a href="mailto:contact@examhub.lk">
                                contact@examhub.lk
                            </a>
                        </li>
                        <li>
                            <span><i class="fas fa-phone"></i> Contact</span><a href="tel:0770508710">0770508710</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="user-login text-right col-md-4">
                @if(empty(Auth::check()))
                    <a href="{{URL_USERS_REGISTER}}" class="cs-nav-btn cs-responsive-menu"><i
                                class="fas fa-edit"></i> {{getPhrase('Register')}}</a>
                    {{--<a class="popup-with-form" href="#register-form">--}}
                    {{--<i class="fas fa-edit"></i> Register--}}
                    {{--</a>--}}
                    <a href="{{URL_USERS_LOGIN}}" class="cs-nav-btn cs-responsive-menu">
                        <i class="fas fa-user " aria-hidden="true"></i>{{getPhrase('Login')}}</a>
                    {{--<a class="popup-with-form" href="#login-form">--}}
                    {{--<i class="fas fa-user"></i> Login--}}
                    {{--</a>--}}
                @else
                    <a href="{{URL_USERS_DASHBOARD}}" class="cs-nav-btn cs-responsive-menu">
                        <i class="fas fa-angle-double-right" aria-hidden="true"></i>{{getPhrase('Dashboard')}}</a>
                    <a href="{{URL_USERS_LOGOUT}}" class="cs-nav-btn cs-responsive-menu">
                        <i class="fas fa-user " aria-hidden="true"></i>{{getPhrase('Logout')}}</a>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- End Header Top -->

<!-- Header
============================================= -->
<header id="home">

    <!-- Start Navigation -->
    <nav class="navbar navbar-default navbar-sticky bootsnav">

        <!-- Start Top Search -->
        <div class="container">
            <div class="row">
                <div class="top-search">
                    <div class="input-group">
                        <form action="#">
                            <input type="text" name="text" class="form-control" placeholder="Search">
                            <button type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Top Search -->

        <div class="container">

            <!-- Start Atribute Navigation -->
            <div class="attr-nav">
                <ul>
                    <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                </ul>
            </div>
            <!-- End Atribute Navigation -->

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <a class="navbar-brand" href="{{URL_HOME }}"><img
                            src="{{IMAGE_PATH_SETTINGS.getSetting('site_logo', 'site_settings')}}" alt="logo"
                            class="cs-logo" class="img-responsive" style="height: 40px;"></a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="#" data-out="#">
                    <li {{ isActive($active_class, 'home') }} ><a href="{{ URL_HOME }}">{{getPhrase('home')}}</a></li>
                    <li {{ isActive($active_class, 'practice_exams') }} ><a
                                href="{{ URL_VIEW_ALL_PRACTICE_EXAMS }}">{{getPhrase('practice_exams')}}</a></li>
{{--                    <li {{ isActive($active_class, 'lms') }} ><a href="{{ URL_VIEW_ALL_LMS_CATEGORIES }}">LMS</a></li>--}}
{{--                    <li {{ isActive($active_class, 'courses') }} ><a--}}
{{--                                href="{{ URL_VIEW_SITE_COURSES }}">{{getPhrase('courses')}}</a></li>--}}
{{--                    <li {{ isActive($active_class, 'pattren') }} ><a--}}
{{--                                href="{{ URL_VIEW_SITE_PATTREN }}">{{getPhrase('pattern')}}</a></li>--}}
                    <li {{ isActive($active_class, 'pricing') }} ><a
                                href="{{ URL_VIEW_SITE_PRICING }}">{{getPhrase('pricing')}}</a></li>
                    <li {{ isActive($active_class, 'syllabus') }} ><a
                                href="{{ URL_VIEW_SITE_SYALLABUS }}">{{getPhrase('syllabus')}}</a></li>
                    {{-- <li {{ isActive($active_class, 'practice_exams') }} ><a href="{{ SITE_PAGES_PRIVACY }}">{{ getPhrase('privacy_and_policy') }}</a></li>
                    <li {{ isActive($active_class, 'practice_exams') }} ><a href="{{ SITE_PAGES_TERMS }}">{{getPhrase('terms_and_conditions')}}</a></li> --}}
                    <li {{ isActive($active_class, 'about-us') }} ><a
                                href="{{ SITE_PAGES_ABOUT_US }}">{{getPhrase('about_us')}}</a></li>

                    <li {{ isActive($active_class, 'contact-us') }} ><a
                                href="{{ URL_SITE_CONTACTUS }}">{{getPhrase('contact_us')}}</a></li>

                    @php
                        $pages = \App\Page::where('status', 1)->get();
                    @endphp

                </ul>
            </div><!-- /.navbar-collapse -->
        </div>

    </nav>
    <!-- End Navigation -->

</header>
<!-- End Header -->