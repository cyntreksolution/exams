<header id="home">
    <nav class="navbar navbar-default navbar-sticky bootsnav">

        <!-- Start Top Search -->
        <div class="container">
            <div class="row">
                <div class="top-search">
                    <div class="input-group">
                        <form action="#">
                            <input type="text" name="text" class="form-control" placeholder="Search">
                            <button type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Top Search -->

        <div class="container">

            <!-- Start Atribute Navigation -->
            <div class="attr-nav">
                <ul>
                    <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                </ul>
            </div>
            <!-- End Atribute Navigation -->

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <a class="navbar-brand" href="{{URL_HOME }}"><img
                            src="{{IMAGE_PATH_SETTINGS.getSetting('site_logo', 'site_settings')}}" alt="logo"
                            class="cs-logo" class="img-responsive" style="height: 40px;"></a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="#" data-out="#">
                    <li {{ isActive($active_class, 'home') }} ><a href="{{ URL_HOME }}">{{getPhrase('home')}}</a></li>
                    <li {{ isActive($active_class, 'practice_exams') }} ><a
                                href="{{ URL_VIEW_ALL_PRACTICE_EXAMS }}">{{getPhrase('exams')}}</a></li>
                    {{--                    <li {{ isActive($active_class, 'lms') }} ><a href="{{ URL_VIEW_ALL_LMS_CATEGORIES }}">LMS</a></li>--}}
                    {{--                    <li {{ isActive($active_class, 'courses') }} ><a--}}
                    {{--                                href="{{ URL_VIEW_SITE_COURSES }}">{{getPhrase('courses')}}</a></li>--}}
                    {{--                    <li {{ isActive($active_class, 'pattren') }} ><a--}}
                    {{--                                href="{{ URL_VIEW_SITE_PATTREN }}">{{getPhrase('pattern')}}</a></li>--}}
                    <li {{ isActive($active_class, 'pricing') }} ><a
                                href="{{ URL_VIEW_SITE_PRICING }}">{{getPhrase('pricing')}}</a></li>
                    <li {{ isActive($active_class, 'syllabus') }} ><a
                                href="{{ URL_VIEW_SITE_SYALLABUS }}">{{getPhrase('syllabus')}}</a></li>
                    {{-- <li {{ isActive($active_class, 'practice_exams') }} ><a href="{{ SITE_PAGES_PRIVACY }}">{{ getPhrase('privacy_and_policy') }}</a></li>
                    <li {{ isActive($active_class, 'practice_exams') }} ><a href="{{ SITE_PAGES_TERMS }}">{{getPhrase('terms_and_conditions')}}</a></li> --}}
                    <li {{ isActive($active_class, 'about-us') }} ><a
                                href="{{ SITE_PAGES_ABOUT_US }}">{{getPhrase('about_us')}}</a></li>

                    <li {{ isActive($active_class, 'contact-us') }} ><a
                                href="{{ URL_SITE_CONTACTUS }}">{{getPhrase('contact_us')}}</a></li>

                    @php
                        $pages = \App\Page::where('status', 1)->get();
                    @endphp

                </ul>
            </div><!-- /.navbar-collapse -->
        </div>

    </nav>
</header>
