<div class="top-bar-area address-two-lines bg-dark text-light">
    <div class="container">
        <div class="row">
            <div class="col-md-8 address-info">
                <div class="info box">
                    <ul>
                        {{--                        <li>--}}
                        {{--                            <span><i class="fas fa-map"></i> Address</span>California, TX 70240--}}
                        {{--                        </li>--}}
                        <li>
                            <span><i class="fas fa-envelope-open"></i> Email</span>
                            <a href="mailto:contact@examhub.lk">
                                contact@examhub.lk
                            </a>
                        </li>
                        <li>
                            <span><i class="fas fa-phone"></i> Contact</span><a href="tel:0770508710">0770508710</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="user-login text-right col-md-4">
                @if(empty(Auth::check()))
                    <a href="{{URL_USERS_REGISTER}}" class="cs-nav-btn cs-responsive-menu"><i
                                class="fas fa-edit"></i> {{getPhrase('Register')}}</a>
                    {{--<a class="popup-with-form" href="#register-form">--}}
                    {{--<i class="fas fa-edit"></i> Register--}}
                    {{--</a>--}}
                    <a href="{{URL_USERS_LOGIN}}" class="cs-nav-btn cs-responsive-menu">
                        <i class="fas fa-user " aria-hidden="true"></i>{{getPhrase('Login')}}</a>
                    {{--<a class="popup-with-form" href="#login-form">--}}
                    {{--<i class="fas fa-user"></i> Login--}}
                    {{--</a>--}}
                @else
                    <a href="{{URL_USERS_DASHBOARD}}" class="cs-nav-btn cs-responsive-menu">
                        <i class="fas fa-angle-double-right" aria-hidden="true"></i>{{getPhrase('Dashboard')}}</a>
                    <a href="{{URL_USERS_LOGOUT}}" class="cs-nav-btn cs-responsive-menu">
                        <i class="fas fa-user " aria-hidden="true"></i>{{getPhrase('Logout')}}</a>
                @endif
            </div>
        </div>
    </div>
</div>