@extends('layouts.admin.adminlayout')

@section('header_scripts')

@stop

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">

                    </ol>
                </div>
            </div>
            <div class="panel panel-custom  col-lg-12">
                <div class="panel-heading">
                    <div class="pull-right messages-buttons">

                    </div>
                    <h1>{{$record->title}}</h1>
                </div>

                <div class="panel-body" id="app">
                    {!! Form::open(array('url' => URL_QUESTIONPAPER_ADD, 'method' => 'POST', 'files' => TRUE, 'name'=>'formQuestionBank ', 'novalidate'=>'', 'class'=>'validation-align')) !!}

{{--                    <button data-toggle="tooltip" data-placement="top" title="Add Option To Question"--}}
{{--                            class="btn btn-info btn-add-more  add-more" id="add_kid()_1" onclick="addkid()" value="+"--}}
{{--                            type="button">+--}}
{{--                    </button>--}}

                    <input type="hidden" name="quize_id" value="{{$record->id}}">
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            {{ Form::label('question', getphrase('question')) }}
                            <span class="text-red">*</span>
                            {{ Form::textarea('question', $value = null , $attributes = array('class'=>'form-control ckeditor', 'placeholder' => 'Your question', 'rows' => '5')) }}
                            <div class="validation-error" ng-messages="formQuestionBank.question.$error">
                                {!! getValidationMessage()!!}
                                {!! getValidationMessage('minlength')!!}
                            </div>
                        </fieldset>
                    </div>

                    <!-- <div id="kids">
                        Child_1:
                        <input type="text" name="child_1">
                        <input type="button" id="add_kid()_1" onclick="addkid()" value="+" />
                      </div> -->


                    <div class="col-md-12" id="kids">
                        <fieldset class="form-group">
                            <label>Option 1 </label> <span class="text-red">*</span>
                            <textarea name="options[]" class="form-control ckeditor" placeholder="Option 1"
                                      required="true">
                            </textarea>
                        </fieldset>
                    </div>


                    <div class="col-md-12" id="kids">
                        <fieldset class="form-group">
                            <label>Option 2 </label> <span class="text-red">*</span>
                            <textarea name="options[]" class="form-control ckeditor" placeholder="Option 1"
                                      required="true">
                            </textarea>
                        </fieldset>
                    </div>

                    <div class="col-md-12" id="kids">
                        <fieldset class="form-group">
                            <label>Option 3 </label> <span class="text-red">*</span>
                            <textarea name="options[]" class="form-control ckeditor" placeholder="Option 1"
                                      required="true">
                            </textarea>
                        </fieldset>
                    </div>

                    <div class="col-md-12" id="kids">
                        <fieldset class="form-group">
                            <label>Option 4 </label> <span class="text-red">*</span>
                            <textarea name="options[]" class="form-control ckeditor" placeholder="Option 1"
                                      required="true">
                            </textarea>
                        </fieldset>
                    </div>

                    <div class="col-md-12" id="kids">
                        <fieldset class="form-group">
                            <label>Option 5 </label> <span class="text-red">*</span>
                            <textarea name="options[]" class="form-control ckeditor" placeholder="Option 1"
                                      required="true">
                            </textarea>
                        </fieldset>
                    </div>

                    <div class="col-md-12" id="kids">
                        <fieldset class="form-group">
                            <label>Option 6 </label> <span class="text-red">*</span>
                            <textarea name="options[]" class="form-control ckeditor" placeholder="Option 1"
                                      required="true">
                            </textarea>
                        </fieldset>
                    </div>


                    <div class="col-md-12">
                        <fieldset class="form-group ">
                            <label>Explanation</label> <span class="text-red">*</span>
                            <textarea name="explanation" class="form-control ckeditor " placeholder="Vivarana"
                                      required="true">
                            </textarea>
                        </fieldset>
                    </div>


                    <div class="col-md-6">
                        <fieldset class="form-group ">
                            <label>Correct Answer</label> <span class="text-red">*</span>
                            <input type="text" name="correct_answers" class="form-control" placeholder="Correct Answer"
                                   required="true">
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset class="form-group ">
                            <label>Time</label> <span class="text-red">*</span>
                            <input type="text" name="time_to_spend" class="form-control" placeholder="Time"
                                   required="true">
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset class="form-group ">
                            <label>Marks</label> <span class="text-red">*</span>
                            <input type="text" name="marks" class="form-control" placeholder="Marks" required="true">
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset class="form-group ">
                            <label>&ensp;</label>
                            <button class="btn btn-lg btn-block btn-success button">Add Question To Paper</button>
                        </fieldset>
                    </div>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
        @endsection





        @section('footer_scripts')
            <script src="http://cdn.ckeditor.com/4.5.8/standard-all/ckeditor.js"></script>
            @include('exams.questionbank.scripts.js-scripts')
            @include('common.validations', array('isLoaded'=>TRUE))
            {{--    @include('common.editor')--}}
            @if($record)
                @if($record->question_type=='video')
                    @include('common.video-scripts')
                @endif
            @endif
        @stop
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

        <script type="text/javascript">
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            function initTextArea(identity2) {
                for (name in CKEDITOR.instances) {
                    CKEDITOR.instances[name].destroy();
                }

                $('.ckeditor').each(function () {
                    CKEDITOR.replace($(this).attr('name'), {
                        extraPlugins: 'mathjax',
                        mathJaxLib: 'http://cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML',
                        height: 320
                    });
                });
            }


            var i = 1;

            function addkid() {
                i++;
                var div = document.createElement('div');
                var id = i;
                let idntity = 'identity' + id;
                // div.innerHTML = 'Option ' + id + ': <fieldset class="form-group"><textarea type="text" class="form-control ckeditor" name="options_' + id + '"/></textarea></fieldset>' + ' <input class="btn btn-danger btn-sm" type="button" id="rem_kid()_' + id + '" onclick="remkid(this)" value="-" /></div>';
                div.innerHTML = 'Option ' + id + ': <fieldset class="form-group"><textarea type="text" class="form-control ckeditor '+idntity+'" name="options[]" id="options_' + id + '"/></textarea></fieldset>';
                document.getElementById('kids').appendChild(div);
                initTextArea(identity2);
            }

            function remkid(div) {
                document.getElementById('kids').removeChild(div.parentNode);
                i--;
            }


        </script>
    </div>
    <style>
        .btn-add-more {
            position: fixed;
            bottom: 50px;
            right: 100px;
            height: 50px;
            width: 50px;
            border-radius: 50px !important;
            z-index: 999;
        }
    </style>

