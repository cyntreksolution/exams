<!DOCTYPE html>
<html lang="en">


<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ========== Page Title ========== -->
    <title> @yield('title') ExamHUB.lk</title>

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{IMAGE_PATH_SETTINGS.getSetting('site_favicon', 'site_settings')}}" type="image/x-icon"/>
    <!-- ========== Start Stylesheet ========== -->
    <link href="{{asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/font-awesome.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/flaticon-set.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/magnific-popup.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/owl.carousel.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/owl.theme.default.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/animate.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/bootsnav.css')}}" rel="stylesheet"/>
    <link href="{{asset('public/assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/css/responsive.css')}}" rel="stylesheet"/>
    <!-- ========== End Stylesheet ========== -->


    <!-- ========== Google Fonts ========== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">

</head>

<body>

<!-- Preloader Start -->
<div class="se-pre-con"></div>
<!-- Preloader Ends -->

@include('themeone::includes.top')
@include('themeone::includes.menu')
<!-- Start Login Form
============================================= -->
<form action="#" id="login-form" class="mfp-hide white-popup-block">
    <div class="col-md-4 login-social">
        <h4>Login with social</h4>
        <ul>
            <li class="facebook">
                <a href="#">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </li>
            <li class="twitter">
                <a href="#">
                    <i class="fab fa-twitter"></i>
                </a>
            </li>
            <li class="linkedin">
                <a href="#">
                    <i class="fab fa-linkedin-in"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-8 login-custom">
        <h4>login to your registered account!</h4>
        <div class="col-md-12">
            <div class="row">
                <div class="form-group">
                    <input class="form-control" placeholder="Email*" type="email">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="form-group">
                    <input class="form-control" placeholder="Password*" type="text">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <label for="login-remember"><input type="checkbox" id="login-remember">Remember Me</label>
                <a title="Lost Password" href="#" class="lost-pass-link">Lost your password?</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <button type="submit">
                    Login
                </button>
            </div>
        </div>
        <p class="link-bottom">Not a member yet? <a href="#">Register now</a></p>
    </div>
</form>
<!-- End Login Form -->

<!-- Start Register Form
============================================= -->
<form action="#" id="register-form" class="mfp-hide white-popup-block">
    <div class="col-md-4 login-social">
        <h4>Register with social</h4>
        <ul>
            <li class="facebook">
                <a href="#">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </li>
            <li class="twitter">
                <a href="#">
                    <i class="fab fa-twitter"></i>
                </a>
            </li>
            <li class="linkedin">
                <a href="#">
                    <i class="fab fa-linkedin-in"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-8 login-custom">
        <h4>Register a new account</h4>
        <div class="col-md-12">
            <div class="row">
                <div class="form-group">
                    <input class="form-control" placeholder="Email*" type="email">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="form-group">
                    <input class="form-control" placeholder="Username*" type="text">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="form-group">
                    <input class="form-control" placeholder="Password*" type="text">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="form-group">
                    <input class="form-control" placeholder="Repeat Password*" type="text">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <button type="submit">
                    Sign up
                </button>
            </div>
        </div>
        <p class="link-bottom">Are you a member? <a href="#">Login now</a></p>
    </div>
</form>
<!-- End Register Form -->

<!-- Start Banner
============================================= -->
<div class="banner-area content-top-heading less-paragraph text-normal">
    <div id="bootcarousel" class="carousel slide animate_text carousel-fade" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner text-light carousel-zoom">
            <div class="item active">
                <div class="slider-thumb bg-fixed" style="background-image: url(public/assets/img/banner/1.jpg);"></div>
                <div class="box-table shadow dark">
                    <div class="box-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="content">
                                        <h3 data-animation="animated slideInLeft">Reach you career</h3>
                                        <h1 data-animation="animated slideInUp">Learn from best online training
                                            course</h1>
                                        <a data-animation="animated slideInUp" class="btn btn-light border btn-md"
                                           href="#">Learn more</a>
                                        <a data-animation="animated slideInUp" class="btn btn-theme effect btn-md"
                                           href="#">View Courses</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="slider-thumb bg-fixed"
                     style="background-image: url('/public/assets/img/banner/2.jpg');"></div>
                <div class="box-table shadow dark">
                    <div class="box-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="content">
                                        <h3 data-animation="animated slideInLeft">We're glade to see you</h3>
                                        <h1 data-animation="animated slideInUp">explore our brilliant graduates</h1>
                                        <a data-animation="animated slideInUp" class="btn btn-light border btn-md"
                                           href="#">Learn more</a>
                                        <a data-animation="animated slideInUp" class="btn btn-theme effect btn-md"
                                           href="#">View Courses</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="slider-thumb bg-fixed" style="background-image: url(assets/img/banner/3.jpg);"></div>
                <div class="box-table shadow dark">
                    <div class="box-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="content">
                                        <h3 data-animation="animated slideInLeft">The goal of education</h3>
                                        <h1 data-animation="animated slideInUp">Join the bigest comunity of eduka</h1>
                                        <a data-animation="animated slideInUp" class="btn btn-light border btn-md"
                                           href="#">Learn more</a>
                                        <a data-animation="animated slideInUp" class="btn btn-theme effect btn-md"
                                           href="#">View Courses</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Wrapper for slides -->

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#bootcarousel" data-slide="prev">
            <i class="fa fa-angle-left"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#bootcarousel" data-slide="next">
            <i class="fa fa-angle-right"></i>
            <span class="sr-only">Next</span>
        </a>

    </div>
</div>
<!-- End Banner -->

<!-- Start Popular Courses
============================================= -->
<div class="popular-courses circle bg-gray carousel-shadow default-padding">
    <div class="container">
        <div class="row">
            <div class="site-heading text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Practice Exams And Exam Categories</h2>
                    <p>
                        Discourse assurance estimable applauded to so. Him everything melancholy uncommonly but
                        solicitude inhabiting projection off. Connection stimulated estimating excellence an to
                        impression.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="popular-courses-items popular-courses-carousel owl-carousel owl-theme">

                    @if(!empty($quizzes))
                        @foreach($quizzes as $quiz)
                            <div class="item">
                                <div class="thumb">
                                    <a href="#">
                                        @if($quiz->image)
                                            <img src="{{IMAGE_PATH_EXAMS.$quiz->image}}" alt="exam">
                                        @else
                                            <img src="{{IMAGE_PATH_EXAMS_DEFAULT}}" alt="exam">
                                        @endif
                                    </a>
                                    <div class="price">Price: {{($quiz->is_paid) ?'Premium':'Free'}}</div>
                                </div>
                                <div class="info">
                                    <h4><a href="#">{{ucfirst($quiz->title)}}</a></h4>
                                    <div class="bottom-info">
                                        <ul>
                                            <li>
                                                <i class="fas fa-chart-line"></i> {{(int)$quiz->total_marks}}
                                            </li>
                                            <li>
                                                <i class="fas fa-clock"></i> {{$quiz->dueration}} mins
                                            </li>

                                            @if($quiz->is_paid)
                                                <li>
                                                    {{getCurrencyCode()}} {{(int)$quiz->cost}}
                                                </li>
                                            @endif
                                        </ul>

                                        @if( $quiz->is_paid == 1)
                                            <a href="{{ URL_START_EXAM_AFTER_LOGIN.$quiz->id }}" class="text-center mt-2">{{getPhrase('Start')}}</a>
                                        @else
                                            <a href="{{ URL_FRONTEND_START_EXAM.$quiz->slug }}" class="text-center mt-2">{{getPhrase('Start')}}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif


                </div>
            </div>
        </div>
    </div>
    @if(count($categories))
        <div class="row text-center">
            <ul class="list-inline top40">
                <li><a href="{{URL_VIEW_ALL_EXAM_CATEGORIES}}"
                       class="btn btn-dark border btn-md">{{getPhrase('Browse_all_exams')}}</a></li>
            </ul>
        </div>
    @endif
</div>

<!-- End Popular Courses -->

<!-- Start Fun Factor
============================================= -->
<div class="fun-factor-area default-padding bottom-less text-center bg-fixed shadow dark-hard"
     style="background-image: url(assets/img/banner/2.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 item">
                <div class="fun-fact">
                    <div class="icon">
                        <i class="flaticon-contract"></i>
                    </div>
                    <div class="info">
                        <span class="timer" data-to="212" data-speed="5000"></span>
                        <span class="medium">National Awards</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 item">
                <div class="fun-fact">
                    <div class="icon">
                        <i class="flaticon-professor"></i>
                    </div>
                    <div class="info">
                        <span class="timer" data-to="128" data-speed="5000"></span>
                        <span class="medium">Best Teachers</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 item">
                <div class="fun-fact">
                    <div class="icon">
                        <i class="flaticon-online"></i>
                    </div>
                    <div class="info">
                        <span class="timer" data-to="8970" data-speed="5000"></span>
                        <span class="medium">Students Enrolled</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 item">
                <div class="fun-fact">
                    <div class="icon">
                        <i class="flaticon-reading"></i>
                    </div>
                    <div class="info">
                        <span class="timer" data-to="640" data-speed="5000"></span>
                        <span class="medium">Cources</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Fun Factor -->

<!-- Start Top Categories
============================================= -->
<div id="top-categories" class="top-cat-area default-padding bottom-less">
    <div class="container">
        <div class="row">
            <div class="site-heading text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Top Categories</h2>
                    <p>
                        Discourse assurance estimable applauded to so. Him everything melancholy uncommonly but
                        solicitude inhabiting projection off. Connection stimulated estimating excellence an to
                        impression.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            @if(isset($lms_cates))
                @foreach($lms_cates as $lms_category)
                    <div class="top-cat-items">
                        <div class="col-md-3 col-sm-6 equal-height">
                            <div class="item" style="background-image: url(assets/img/category/1.jpg);">
                                <a href="{{URL_VIEW_ALL_LMS_CATEGORIES.'/'.$lms_category->slug}}">
                                    <i class="flaticon-feature"></i>
                                    <div class="info">
                                        <h4>{{$lms_category->category}}</h4>
                                        {{--                                        <span>(1,226) Topics</span>--}}
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <h4>{{getPhrase('no_categories_are_available')}}</h4>
            @endif
        </div>
    </div>
    @if(isset($lms_cates))

        <div class="row text-center">
            <ul class="list-inline top40">
                <li><a href="{{URL_VIEW_ALL_LMS_CATEGORIES}}"
                       class="btn btn-dark border btn-md">{{getPhrase('Browse_all_categories')}}</a></li>
            </ul>
        </div>

    @endif
</div>
<!-- End Top Categories -->
<!-- Start Why Chose Us
============================================= -->


<div class="wcs-area bg-dark text-light">
    <div class="container-full">
        <div class="row">
            <div class="col-md-6 thumb bg-cover" style="background-image: url(assets/img/banner/16.jpg);"></div>
            <div class="col-md-6 content">
                <div class="site-heading text-left">
                    <h2>Why chose us</h2>
                    <p>
                        Discourse assurance estimable applauded to so. Him everything melancholy uncommonly but
                        solicitude inhabiting projection off. Connection stimulated estimating excellence an to
                        impression.
                    </p>
                </div>

                <!-- item -->
                <div class="item">
                    <div class="icon">
                        <i class="flaticon-trending"></i>
                    </div>
                    <div class="info">
                        <h4>
                            <a href="#">Trending Courses</a>
                        </h4>
                        <p>
                            Absolute required of reserved in offering no. How sense found our those gay again taken the.
                            Had mrs outweigh desirous sex overcame. Improved property reserved disposal do offering me.
                        </p>
                    </div>
                </div>
                <!-- item -->

                <!-- item -->
                <div class="item">
                    <div class="icon">
                        <i class="flaticon-books"></i>
                    </div>
                    <div class="info">
                        <h4>
                            <a href="#">Books & Library</a>
                        </h4>
                        <p>
                            Absolute required of reserved in offering no. How sense found our those gay again taken the.
                            Had mrs outweigh desirous sex overcame. Improved property reserved disposal do offering me.
                        </p>
                    </div>
                </div>
                <!-- item -->

                <!-- item -->
                <div class="item">
                    <div class="icon">
                        <i class="flaticon-professor"></i>
                    </div>
                    <div class="info">
                        <h4>
                            <a href="#">Certified Teachers</a>
                        </h4>
                        <p>
                            Absolute required of reserved in offering no. How sense found our those gay again taken the.
                            Had mrs outweigh desirous sex overcame. Improved property reserved disposal do offering me.
                        </p>
                    </div>
                </div>
                <!-- item -->

            </div>
        </div>
    </div>
</div>
<!-- End Why Chose Us -->

<!-- Start Featured Courses
============================================= -->
{{--<div id="featured-courses" class="featured-courses-area left-details default-padding">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="featured-courses-carousel owl-carousel owl-theme">--}}
{{--                <!-- Start Single Item -->--}}
{{--                <div class="item">--}}
{{--                    <div class="col-md-5">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="assets/img/courses/f1.jpg" alt="Thumb">--}}
{{--                            <div class="live-view">--}}
{{--                                <a href="assets/img/courses/f1.jpg" class="item popup-link">--}}
{{--                                    <i class="fa fa-camera"></i>--}}
{{--                                </a>--}}
{{--                                <a class="popup-youtube" href="https://www.youtube.com/watch?v=vQqZIFCab9o">--}}
{{--                                    <i class="fa fa-video"></i>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-7 info">--}}
{{--                        <h2>--}}
{{--                            <a href="#">Codecademy software programming</a>--}}
{{--                        </h2>--}}
{{--                        <h4>featured courses</h4>--}}
{{--                        <p>--}}
{{--                            Both rest of know draw fond post as. It agreement defective to excellent. Feebly do engage--}}
{{--                            of narrow. Extensive repulsive belonging depending if promotion be zealously as. Preference--}}
{{--                            inquietude ask--}}
{{--                        </p>--}}
{{--                        <h3>Learning outcomes</h3>--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Over 37 lectures and 55.5 hours of content!</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Testing Training Included.</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Course content designed by considering current software testing</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Practical assignments at the end of every session.</span>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <a href="#" class="btn btn-theme effect btn-md" data-animation="animated slideInUp">Enroll--}}
{{--                            Now</a>--}}
{{--                        <div class="bottom-info align-left">--}}
{{--                            <div class="item">--}}
{{--                                <h4>Author</h4>--}}
{{--                                <a href="#">--}}
{{--                                    <span>Devid Honey</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <h4>Students enrolled</h4>--}}
{{--                                <span>5455</span>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <h4>Rating</h4>--}}
{{--                                <span class="rating">--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star-half-alt"></i>--}}
{{--                                    </span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- End Single Item -->--}}
{{--                <!-- Start Single Item -->--}}
{{--                <div class="item">--}}
{{--                    <div class="col-md-5">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="assets/img/courses/f2.jpg" alt="Thumb">--}}
{{--                            <div class="live-view">--}}
{{--                                <a href="assets/img/courses/f2.jpg" class="item popup-link">--}}
{{--                                    <i class="fa fa-camera"></i>--}}
{{--                                </a>--}}
{{--                                <a class="popup-youtube" href="https://www.youtube.com/watch?v=vQqZIFCab9o">--}}
{{--                                    <i class="fa fa-video"></i>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-7 info">--}}
{{--                        <h2>--}}
{{--                            <a href="#">Data analysis and data science</a>--}}
{{--                        </h2>--}}
{{--                        <h4>featured courses</h4>--}}
{{--                        <p>--}}
{{--                            Both rest of know draw fond post as. It agreement defective to excellent. Feebly do engage--}}
{{--                            of narrow. Extensive repulsive belonging depending if promotion be zealously as. Preference--}}
{{--                            inquietude ask--}}
{{--                        </p>--}}
{{--                        <h3>Learning outcomes</h3>--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Over 37 lectures and 55.5 hours of content!</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Testing Training Included.</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Course content designed by considering current software testing</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Practical assignments at the end of every session.</span>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <a href="#" class="btn btn-theme effect btn-md" data-animation="animated slideInUp">Enroll--}}
{{--                            Now</a>--}}
{{--                        <div class="bottom-info align-left">--}}
{{--                            <div class="item">--}}
{{--                                <h4>Author</h4>--}}
{{--                                <a href="#">--}}
{{--                                    <span>Devid Honey</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <h4>Students enrolled</h4>--}}
{{--                                <span>5455</span>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <h4>Rating</h4>--}}
{{--                                <span class="rating">--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star-half-alt"></i>--}}
{{--                                    </span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- End Single Item -->--}}
{{--                <!-- Start Single Item -->--}}
{{--                <div class="item">--}}
{{--                    <div class="col-md-5">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="assets/img/courses/f3.jpg" alt="Thumb">--}}
{{--                            <div class="live-view">--}}
{{--                                <a href="assets/img/courses/f3.jpg" class="item popup-link">--}}
{{--                                    <i class="fa fa-camera"></i>--}}
{{--                                </a>--}}
{{--                                <a class="popup-youtube" href="https://www.youtube.com/watch?v=vQqZIFCab9o">--}}
{{--                                    <i class="fa fa-video"></i>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-7 info">--}}
{{--                        <h2>--}}
{{--                            <a href="#">Graphic and interactive design</a>--}}
{{--                        </h2>--}}
{{--                        <h4>featured courses</h4>--}}
{{--                        <p>--}}
{{--                            Both rest of know draw fond post as. It agreement defective to excellent. Feebly do engage--}}
{{--                            of narrow. Extensive repulsive belonging depending if promotion be zealously as. Preference--}}
{{--                            inquietude ask--}}
{{--                        </p>--}}
{{--                        <h3>Learning outcomes</h3>--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Over 37 lectures and 55.5 hours of content!</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Testing Training Included.</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Course content designed by considering current software testing</span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class="fas fa-check-double"></i>--}}
{{--                                <span>Practical assignments at the end of every session.</span>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <a href="#" class="btn btn-theme effect btn-md" data-animation="animated slideInUp">Enroll--}}
{{--                            Now</a>--}}
{{--                        <div class="bottom-info align-left">--}}
{{--                            <div class="item">--}}
{{--                                <h4>Author</h4>--}}
{{--                                <a href="#">--}}
{{--                                    <span>Devid Honey</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <h4>Students enrolled</h4>--}}
{{--                                <span>5455</span>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <h4>Rating</h4>--}}
{{--                                <span class="rating">--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star"></i>--}}
{{--                                        <i class="fas fa-star-half-alt"></i>--}}
{{--                                    </span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- End Single Item -->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- End Featured Courses -->--}}


<!-- Start About
============================================= -->
<div class="about-area default-padding">
    <div class="container">
        <div class="row">
            <div class="about-info">
                <div class="col-md-6 thumb">
                    <img src="assets/img/about.jpg" alt="Thumb">
                </div>
                <div class="col-md-6 info">
                    <h5>Introduction</h5>
                    <h2>Welcome to the beigest online learning source of Eduka</h2>
                    <p>
                        Alteration literature to or an sympathize mr imprudence. Of is ferrars subject as enjoyed or
                        tedious cottage. Procuring as in resembled by in agreeable. Next long no gave mr eyes.
                        Admiration advantages no he celebrated so pianoforte unreserved. Not its herself forming charmed
                        amiable. Him why feebly expect future now.
                    </p>
                    <p>
                        Curiosity incommode now led smallness allowance. Favour bed assure son things yet. She consisted
                        consulted elsewhere happiness disposing household any old the. Widow downs. Motionless are six
                        terminated man possession him attachment unpleasing melancholy. Sir smile arose one share. No
                        abroad in easily relied an whence lovers temper by.
                    </p>
                    <a href="#" class="btn btn-dark border btn-md">Read More</a>
                </div>
            </div>
            <div class="seperator col-md-12">
                <span class="border"></span>
            </div>
            <div class="our-features">
                <div class="col-md-4 col-sm-4">
                    <div class="item mariner">
                        <div class="icon">
                            <i class="flaticon-faculty-shield"></i>
                        </div>
                        <div class="info">
                            <h4>Expert faculty</h4>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="item java">
                        <div class="icon">
                            <i class="flaticon-book-2"></i>
                        </div>
                        <div class="info">
                            <h4>Online learning</h4>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="item malachite">
                        <div class="icon">
                            <i class="flaticon-education"></i>
                        </div>
                        <div class="info">
                            <h4>Scholarship</h4>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End About -->


<!-- Start Testimonials
============================================= -->
<div class="testimonials-area carousel-shadow default-padding bg-dark text-light">
    <div class="container">
        <div class="row">
            <div class="site-heading text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Students Review</h2>
                    <p>
                        Able an hope of body. Any nay shyness article matters own removal nothing his forming. Gay own
                        additions education satisfied the perpetual. If he cause manor happy. Without farther she
                        exposed saw man led. Along on happy could cease green oh.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="clients-review-carousel owl-carousel owl-theme">
                    @foreach ($testimonies as $testmony)
                        <div class="item">
                            <div class="col-md-5 thumb">
                                <img src="{{getProfilePath($testmony->image, 'thumb') }}" alt="Thumb">
                            </div>
                            <div class="col-md-7 info">
                                <p>
                                    {{$testmony->description}}
                                </p>
                                <h4>{{$testmony->name}}</h4>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Testimonials -->

<!-- Start Registration
============================================= -->
<div class="reg-area default-padding-top bg-gray">
    <div class="container">
        <div class="row">
            <div class="reg-items">
                <div class="col-md-6 reg-form default-padding-bottom">
                    <div class="site-heading text-left">
                        <h2>Get a Free online Registration</h2>
                        <p>
                            written on charmed justice is amiable farther besides. Law insensible middletons unsatiable
                            for apartments boy delightful unreserved.
                        </p>
                    </div>
                    <form action="#">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="First Name" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Last Name" type="text">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email*" type="email">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Chose Subject</option>
                                        <option>Computer Engineering</option>
                                        <option>Accounting Technologies</option>
                                        <option>Web Development</option>
                                        <option>Machine Language</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Phone" type="text">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit">
                                    Rigister Now
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 thumb">
                    <img src="assets/img/contact.png" alt="Thumb">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Registration -->

@include('themeone::includes.footer')
<!-- Start Popular Courses
============================================= -->
{{--<div class="popular-courses circle bg-gray carousel-shadow default-padding">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="site-heading text-center">--}}
{{--                <div class="col-md-8 col-md-offset-2">--}}
{{--                    <h2>Popular Courses</h2>--}}
{{--                    <p>--}}
{{--                        Discourse assurance estimable applauded to so. Him everything melancholy uncommonly but--}}
{{--                        solicitude inhabiting projection off. Connection stimulated estimating excellence an to--}}
{{--                        impression.--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12">--}}
{{--                <div class="popular-courses-items popular-courses-carousel owl-carousel owl-theme">--}}

{{--                    @if(isset($lms_series))--}}

{{--                        @foreach($lms_series as $series)--}}
{{--                            <div class="item">--}}
{{--                                <div class="thumb">--}}
{{--                                    <a href="{{URL_VIEW_LMS_CONTENTS.$series->slug}}">--}}
{{--                                        @if($series->image)--}}
{{--                                            <img src="{{IMAGE_PATH_UPLOAD_LMS_SERIES.$series->image}}" alt="exam">--}}
{{--                                        @else--}}
{{--                                            <img src="{{IMAGE_PATH_EXAMS_DEFAULT}}" alt="exam">--}}
{{--                                        @endif--}}
{{--                                    </a>--}}
{{--                                    <div class="price">Price: {{($series->is_paid) ?'Premium':'Free'}}</div>--}}
{{--                                </div>--}}
{{--                                <div class="info">--}}
{{--                                    <h4><a href="#">{{ucfirst($series->title)}}</a></h4>--}}
{{--                                    <div class="bottom-info">--}}
{{--                                        <ul>--}}
{{--                                            <li>--}}
{{--                                                <i class="fas fa-chart-line"></i> {{(int)$quiz->total_marks}}--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <i class="fas fa-clock"></i> {{$series->total_items}}--}}
{{--                                            </li>--}}

{{--                                            @if($series->is_paid)--}}
{{--                                                <li>--}}
{{--                                                    {{getPhrase('price')}}: {{getCurrencyCode()}} {{(int)$series->cost}}--}}

{{--                                                </li>--}}
{{--                                            @endif--}}


{{--                                        </ul>--}}
{{--                                        <a href="{{URL_VIEW_LMS_CONTENTS.$series->slug}}">View</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    @endif--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- End Popular Courses -->

@include('themeone::includes.menu')


<script src="{{asset('public/assets/js/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('public/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/assets/js/equal-height.min.js')}}"></script>
<script src="{{asset('public/assets/js/jquery.appear.js')}}"></script>
<script src="{{asset('public/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('public/assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('public/assets/js/modernizr.custom.13711.js')}}"></script>
<script src="{{asset('public/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('public/assets/js/wow.min.js')}}"></script>
<script src="{{asset('public/assets/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('public/assets/js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('public/assets/js/count-to.js')}}"></script>
<script src="{{asset('public/assets/js/bootsnav.js')}}"></script>
<script src="{{asset('public/assets/js/main.js')}}"></script>

</body>

</html>