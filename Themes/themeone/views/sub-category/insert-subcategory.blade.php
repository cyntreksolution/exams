@extends('layouts.admin.adminlayout')
 

@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li><a href="{{PREFIX}}"><i class="mdi mdi-home"></i></a> </li>
                            <li><a href="{{URL_SUB_CATEGORIES}}">{{ getPhrase('add_subcategories')}}</a> </li>
                            <li class="active">{{isset($title) ? $title : ''}}</li>
                        </ol>
                    </div>
                </div>
                @include('errors.errors')   
                <div class="panel panel-custom col-lg-5" >
                    <div class="panel-heading">
                        <div class="pull-right messages-buttons">
                            <a href="{{URL_SUB_CATEGORIES}}" class="btn  btn-primary button" >{{ getPhrase('list')}}</a>
                        </div>
                    <h1>{{ $title }}  </h1>
                    </div>
                    <div class="panel-body  form-auth-style" >
                    <?php $button_name = getPhrase('create'); ?>
                    @if ($record)
                     <?php $button_name = getPhrase('update'); ?>
                        {{ Form::model($record, 
                        array('url' => URL_SUB_CATEGORY_EDIT.'/'.$record->slug, 
                        'method'=>'patch', 'files' => true, 'novalidate'=>'','name'=>'formCategories')) }}
                    @else
                        {!! Form::open(array('url' => URL_SUB_CATEGORY_ADD, 'method' => 'POST', 'files' => true, 'novalidate'=>'','name'=>'formCategories')) !!}
                    @endif

                     @include('sub-category.form_elements', 
                     array('button_name'=> $button_name),
                     array('record' => $record))
                    {!! Form::close() !!}
                    </div>


                </div>
                <div class="panel panel-custom col-lg-5" style="margin-left: 50px;">
                    <div class="panel-heading">
                        <div class="pull-right messages-buttons">
                            <a href="{{URL_SUB_CATEGORIES}}" class="btn  btn-primary button" >{{ getPhrase('list')}}</a>
                        </div>
                    <h1>{{ $title }}  </h1>
                    </div>
                    <div class="panel-body  form-auth-style" >
                    <?php $button_name = getPhrase('create'); ?>
                    @if ($record)
                     <?php $button_name = getPhrase('update'); ?>
                        {{ Form::model($record, 
                        array('url' => URL_SUB_CATEGORY_EDIT.'/'.$record->slug, 
                        'method'=>'patch', 'files' => true, 'novalidate'=>'','name'=>'formCategories')) }}
                    @else
                        {!! Form::open(array('url' => URL_SUB_CATEGORY_STORE, 'method' => 'POST', 'files' => true, 'novalidate'=>'','name'=>'formCategories')) !!}
                    @endif

                     @include('sub-category.form_subelements', 
                     array('button_name'=> $button_name),
                     array('sub_categories'=> $sub_categories),
                     array('record' => $record))
                    {!! Form::close() !!}
                    </div>
                    

                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@stop
@section('footer_scripts')
  @include('common.validations');
  @include('common.alertify')
 
@stop
 