 					 
					<fieldset class="form-group col-md-6">

								{{ Form::label('sub_categories', getphrase('sub_categories')) }}

								<span class="text-red">*</span>

								{{Form::select('parent_id', $sub_categories, null, ['class'=>'form-control', 'ng-model' => 'id', 

								'placeholder' => 'Select', 

								'ng-change'=>'categoryChanged(id)' ])}}

							</fieldset>

 					 <fieldset class="form-group">
						
						{{ Form::label('category', getphrase('Sub_category_name')) }}
						<span class="text-red">*</span>
						{{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_sub_category_name'),
							'ng-model'=>'category', 
							'id' => 'subname',
							'ng-pattern' => getRegexPattern('name'),
							'ng-minlength' => '2',
							'ng-maxlength' => '60',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formCategories.category.$touched && formCategories.category.$invalid}',
							 
							)) }}
							<div class="validation-error" ng-messages="formCategories.category.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
	    					{!! getValidationMessage('pattern')!!}
						</div>
					</fieldset>
 					<fieldset class="form-group">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description')) }}
					</fieldset>

				  
					
						<div class="buttons text-center">
							<button class="btn btn-lg btn-success button"
							ng-disabled=''>{{ $button_name }}</button>
						</div>
		 